package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"models"
	"net/http"
	"pkg/gredis"
	"pkg/logging"
	"pkg/setting"
	"routers"
)

func init() {
	setting.Setup()
	models.Setup()
	gredis.Setup()
	logging.Setup()
}

func main() {
	// 部署环境 debug|release
	gin.SetMode(setting.ServerSetting.RunMode)

	routersInit := routers.InitRouter()
	readTimeout := setting.ServerSetting.ReadTimeout
	writeTimeout := setting.ServerSetting.WriteTimeout
	endPoint := fmt.Sprintf(":%d", setting.ServerSetting.HttpPort)
	maxHeaderBytes := 1 << 20

	server := &http.Server{
		Addr:           endPoint,
		Handler:        routersInit,
		ReadTimeout:    readTimeout,
		WriteTimeout:   writeTimeout,
		MaxHeaderBytes: maxHeaderBytes,
	}

	logging.Infof("start http server listening %s", endPoint)
	server.ListenAndServe()

}
