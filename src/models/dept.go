package models

import "github.com/jinzhu/gorm"

type Dept struct {
	Model

	PID  int    `json:"p_id"`
	Name string `json:"name"`
}

type DeptTree struct {
	Model
	PID      int         `json:"p_id"`
	Name     string      `json:"name"`
	Children []*DeptTree `json:"child"`
}

// 部门列表
func GetDepts(maps interface{}) ([]*Dept, error) {
	var depts []*Dept
	err := db.Where(maps).Find(&depts).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	return depts, nil
}
