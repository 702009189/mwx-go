package e

// 系统状态码
const (
	SUCCESS        = 200
	ERROR          = 500
	INVALID_PARAMS = 400
	NOT_FOUND      = 404

	// 1XXXX
	ERROR_GET_AUTHS_FAIL        = 10001 // 账号
	ERROR_ADD_AUTH_FAIL         = 10002
	ERROR_COUNT_AUTH_FAIL       = 10003
	ERROR_CHECK_EXIST_AUTH_FAIL = 10004
	ERROR_NOT_EXIST_AUTH        = 10005
	ERROR_GET_AUTH_FAIL         = 10006
	ERROR_EDIT_AUTH_FAIL        = 10007
	ERROR_DELETE_AUTH_FAIL      = 10008
	ERROR_EXPORT_AUTH_FAIL      = 10009
	ERROR_IMPORT_AUTH_FAIL      = 10010

	ERROR_GET_DEPTS_FAIL = 10101 // 部门

	// 2XXXX
	ERROR_CHECK_TOKEN_FAIL    = 20001
	ERROR_CHECK_TOKEN_TIMEOUT = 20002
	ERROR_ADD_TOKEN           = 20003
	ERROR_LOGIN_TOKEN_FAIL    = 20004
	ERROR_TOKEN_NOT_FIND      = 20005
	ERROR_GET_TOKEN_FAILT     = 20006

	// 3XXXX
	ERROR_UPLOAD_CHECK_IMAGE_FORMAT = 30001
	ERROR_UPLOAD_CHECK_IMAGE_FAIL   = 30002
	ERROR_UPLOAD_SAVE_IMAGE_FAIL    = 30003
	ERROR_UPLOAD_CHECK_EXPORT_FAIL  = 30004
	ERROR_UPLOAD_CHECK_EXCEL_FAIL   = 30005
)

// 系统消息
var MsgFlags = map[int]string{
	SUCCESS:   "ok",
	ERROR:     "fail",
	NOT_FOUND: "404, page not exists!",

	// 1XXXX
	INVALID_PARAMS:              "请求参数错误",
	ERROR_GET_AUTHS_FAIL:        "获取所有账号失败",
	ERROR_ADD_AUTH_FAIL:         "新增账号失败",
	ERROR_COUNT_AUTH_FAIL:       "统计账号失败",
	ERROR_CHECK_EXIST_AUTH_FAIL: "检查账号是否存在失败",
	ERROR_NOT_EXIST_AUTH:        "该账号不存在",
	ERROR_GET_AUTH_FAIL:         "获取单个账号失败",
	ERROR_EDIT_AUTH_FAIL:        "修改账号失败",
	ERROR_DELETE_AUTH_FAIL:      "删除失败",
	ERROR_EXPORT_AUTH_FAIL:      "导出账号失败",
	ERROR_IMPORT_AUTH_FAIL:      "导入账号失败",

	ERROR_GET_DEPTS_FAIL: "获取所有部门失败",

	// 2XXXX
	ERROR_CHECK_TOKEN_FAIL:    "Token鉴权失败",
	ERROR_CHECK_TOKEN_TIMEOUT: "Token已超时",
	ERROR_ADD_TOKEN:           "Token生成失败",
	ERROR_LOGIN_TOKEN_FAIL:    "Token登录失败",
	ERROR_TOKEN_NOT_FIND:      "Token不能为空",
	ERROR_GET_TOKEN_FAILT:     "Token换取用户失败",

	// 3XXXX
	ERROR_UPLOAD_CHECK_IMAGE_FORMAT: "校验图片错误，图片格式或大小有问题",
	ERROR_UPLOAD_CHECK_IMAGE_FAIL:   "检查图片路径失败",
	ERROR_UPLOAD_SAVE_IMAGE_FAIL:    "保存图片失败",
	ERROR_UPLOAD_CHECK_EXPORT_FAIL:  "检查导出路径失败",
	ERROR_UPLOAD_CHECK_EXCEL_FAIL:   "非.xlsx格式文件",
}

// GetMsg get error information based on Code
func GetMsg(code int) string {
	msg, ok := MsgFlags[code]
	if ok {
		return msg
	}

	return MsgFlags[ERROR]
}
