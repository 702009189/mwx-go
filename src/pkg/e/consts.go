package e

import "runtime"

// 系统常量
const (
	Page            = "page"            // 分页
	List            = "list"            // 分页数据列表
	Total           = "total"           // 分页数据统计
	LIST            = "LIST"            // 缓存
	Image_url       = "image_url"       // 图像路径
	Image_save_url  = "image_save_url"  // 图像保存路径
	Xlsx            = ".xlsx"           // excel文件扩展名
	Export_url      = "export_url"      // 导出路径
	Export_save_url = "export_save_url" // 导出保存路径
	Os_name         = runtime.GOOS      // 系统 目录
	Windows         = "windows"         // windows 系统
)
