package export

import "pkg/setting"

// excel 相对保存路径
func GetExcelPath() string {
	return setting.AppSetting.ExportSavePath
}

// excel 完全访问路径
func GetExcelFullUrl(name string) string {
	return setting.AppSetting.PrefixUrl + "/" + GetExcelPath() + name
}

// excel 完整保存路径
func GetExcelFullPath() string {
	return setting.AppSetting.RuntimeRootPath + GetExcelPath()
}
