package upload

import (
	"pkg/file"
	"pkg/setting"
	"strings"
)

// 检查excel文件扩展名 .xlsx
func CheckExcelExt(fileName string) bool {
	ext := file.GetExt(fileName)
	for _, allowExc := range setting.AppSetting.ExcelAllowExts {
		if strings.ToUpper(allowExc) == strings.ToUpper(ext) {
			return true
		}
	}
	return false
}
