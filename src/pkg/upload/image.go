package upload

import (
	"mime/multipart"
	"path"
	"pkg/file"
	"pkg/logging"
	"pkg/setting"
	"strings"
	"util"
)

// 图像保存路径
func GetImagePath() string {
	return setting.AppSetting.ImageSavePath
}

// 图像完整保存路径
func GetImageFullPath() string {
	return setting.AppSetting.RuntimeRootPath + GetImagePath()
}

// 图像名称
func GetImageName(name string) string {
	ext := path.Ext(name)
	fileName := strings.TrimSuffix(name, ext)
	fileName = util.EncodeMD5(fileName)
	return fileName + ext
}

// 检查图像文件扩展名 .jpg,.jpeg,.png
func CheckImageExt(fileName string) bool {
	ext := file.GetExt(fileName)
	for _, allowExt := range setting.AppSetting.ImageAllowExts {
		if strings.ToUpper(allowExt) == strings.ToUpper(ext) {
			return true
		}
	}
	return false
}

// 检查图像大小
func CheckImageSize(f multipart.File) bool {
	size, err := file.GetSize(f)
	if err != nil {
		logging.Error(err)
		return false
	}
	return size <= setting.AppSetting.ImageMaxSize
}

// 图像url
func GetImageFullUrl(name string) string {
	return setting.AppSetting.PrefixUrl + "/" + GetImagePath() + name
}
