package app

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"pkg/e"
)

type Gin struct {
	C *gin.Context
}

type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

// 自定义
func (g *Gin) Response(httpCode, errCode int, data interface{}) {
	g.C.JSON(httpCode, Response{
		Code: httpCode,
		Msg:  e.GetMsg(errCode),
		Data: data,
	})
	return
}

// 200 成功
func (g *Gin) Response200(data interface{}) {
	g.C.JSON(http.StatusOK, Response{
		Code: http.StatusOK,
		Msg:  e.GetMsg(e.SUCCESS),
		Data: data,
	})
	return
}

// 200 成功 定义消息code
func (g *Gin) Response200code(errCode int, data interface{}) {
	g.C.JSON(http.StatusOK, Response{
		Code: http.StatusOK,
		Msg:  e.GetMsg(errCode),
		Data: data,
	})
	return
}

// 400 参数绑定
func (g *Gin) Response400(data interface{}) {
	g.C.JSON(http.StatusBadRequest, Response{
		Code: http.StatusBadRequest,
		Msg:  e.GetMsg(e.INVALID_PARAMS),
		Data: data,
	})
	return
}

// 400 参数绑定 定义消息code
func (g *Gin) Response400code(errCode int, data interface{}) {
	g.C.JSON(http.StatusBadRequest, Response{
		Code: http.StatusBadRequest,
		Msg:  e.GetMsg(errCode),
		Data: data,
	})
	return
}

// 500 异常
func (g *Gin) Response500(errCode int, data interface{}) {
	g.C.JSON(http.StatusInternalServerError, Response{
		Code: http.StatusInternalServerError,
		Msg:  e.GetMsg(errCode),
		Data: data,
	})
	return
}
