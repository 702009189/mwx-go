package app

import (
	"github.com/gin-gonic/gin"
	"middleware/validation"
	"net/http"
	"pkg/e"
)

func BindAndValid(c *gin.Context, form interface{}) (int, int, interface{}) {

	err := c.Bind(form)
	if err != nil {
		return http.StatusBadRequest, e.INVALID_PARAMS, err
	}

	vaild := validation.Validation{}
	check, err := vaild.Valid(form)
	if err != nil {
		return http.StatusInternalServerError, e.ERROR, err
	}

	if !check {
		MarkErrors(vaild.Errors)
		return http.StatusBadRequest, e.INVALID_PARAMS, vaild.Errors
	}

	return http.StatusOK, e.SUCCESS, nil
}
