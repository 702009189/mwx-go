package setting

import (
	"github.com/go-ini/ini"
	"log"
	"pkg/e"
	"time"
)

// 应用配置
type App struct {
	LinuxRootPath   string   // linux目录
	PageSize        int      // 页容量
	TimeFormat      string   // 格式化时间 20060102
	DateFormat      string   // 格式化时间 2006-01-02 15:04:05
	RuntimeRootPath string   // 环境目录
	JwtSecret       string   // jwt 加盐
	ImageSavePath   string   // 图像保存目录
	ImageAllowExts  []string // 图像文件扩展名
	ImageMaxSize    int      // 图像文件大小 MB
	PrefixUrl       string   // 本地项目路径
	ExportSavePath  string   // 导出保存目录
	ExcelAllowExts  []string // excel文件扩展名
	LogPath         string   // 日志目录
}

var AppSetting = &App{}

// 服务配置
type Server struct {
	RunMode      string        // 运行模式
	HttpPort     int           // 端口号
	ReadTimeout  time.Duration // 读取超时时间
	WriteTimeout time.Duration // 写入超时时间
}

var ServerSetting = &Server{}

// mysql
type Database struct {
	Type        string // 数据库类型
	User        string // 用户名
	Password    string // 密码
	Host        string // 主机
	Name        string // 数据库名
	TablePrefix string // 表前缀
}

var DatabaseSetting = &Database{}

// redis
type Redis struct {
	Host        string        // 主机
	Password    string        // 密码
	MaxIdle     int           // 最大空闲
	MaxActive   int           // 最大活动数
	IdleTimeout time.Duration // 超时时间
}

var RedisSetting = &Redis{}

var cfg *ini.File

// Setup initialize the configuration instance
func Setup() {
	var (
		err  error
		path string
	)

	path = "/app/www/mwx-go/runtime/conf/"
	if e.Os_name == e.Windows {
		path = "runtime/conf/"
	}

	cfg, err = ini.Load(path + "app.ini")

	// 校验加载目录
	if err != nil {
		log.Fatalf("setting.Setup, fail to parse 'runtime/conf/app.ini': %v", err)
	}

	mapTo("app", AppSetting)
	mapTo("server", ServerSetting)
	mapTo("database", DatabaseSetting)
	mapTo("redis", RedisSetting)

	AppSetting.ImageMaxSize = AppSetting.ImageMaxSize * 1024 * 1024 // 图像大小限制
	ServerSetting.ReadTimeout = ServerSetting.ReadTimeout * time.Second
	ServerSetting.WriteTimeout = ServerSetting.ReadTimeout * time.Second
	RedisSetting.IdleTimeout = RedisSetting.IdleTimeout * time.Second
	if e.Os_name == e.Windows {
		AppSetting.LinuxRootPath = ""
	}
	AppSetting.RuntimeRootPath = AppSetting.LinuxRootPath + AppSetting.RuntimeRootPath
}

func mapTo(section string, v interface{}) {
	err := cfg.Section(section).MapTo(v)
	if err != nil {
		log.Fatal("Cfg.MapTo RedisSetting err: %v", err)
	}
}
