package cache_service

import (
	"pkg/e"
	"strings"
)

// 页面查询条件
type Dept struct {
	ID   int
	PID  int
	name string
}

// 列表
func (a *Dept) GetDeptsKey() string {
	keys := []string{
		e.CACH_DEPT,
		e.LIST,
	}
	return strings.Join(keys, "_")
}
