package cache_service

import (
	"pkg/e"
	"strconv"
	"strings"
)

// 页面查询条件
type Auth struct {
	ID         int
	Username   string
	Password   string
	Mobile     string
	ModifiedBy string

	PageNum  int
	PageSize int
}

// 单个
func (a *Auth) GetAuthKey() string {
	return e.CACHE_AUTH + "_" + strconv.Itoa(a.ID)
}

// 列表
func (a *Auth) GetAuthsKey() string {
	keys := []string{
		e.CACHE_AUTH,
		e.LIST,
	}

	if a.PageNum > 0 {
		keys = append(keys, strconv.Itoa(a.PageNum))
	}

	if a.PageSize > 0 {
		keys = append(keys, strconv.Itoa(a.PageSize))
	}

	return strings.Join(keys, "_")
}
