package util

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"pkg/e"
	"pkg/setting"
	"strconv"
)

// 当前登录账号
func GetCurrentAuth(c *gin.Context) (*Claims, error) {
	token := c.Query("token")
	if token == "" {
		token = c.GetHeader("token")
	}

	// 解析token
	claims, err := ParseToken(token)
	if err != nil {
		return nil, err
	}
	return claims, nil
}

// 当前登录账号id
func GetCurrentAuthID(c *gin.Context) (string, error) {
	token := c.Query("token")
	if token == "" {
		token = c.GetHeader("token")
	}

	// 解析token
	claims, err := ParseToken(token)
	if err != nil {
		return "", err
	}
	return strconv.Itoa(claims.ID), nil
}

// 服务器路径
func GetPath(c *gin.Context) *string {
	var path string
	if e.Os_name == e.Windows {
		path = fmt.Sprintf("http://%s:%d", c.ClientIP(), setting.ServerSetting.HttpPort)
	} else {
		path = "http://go.acesky.xyz"
	}
	return &path
}
