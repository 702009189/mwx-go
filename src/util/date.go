package util

import (
	"pkg/setting"
	"time"
)

// 精确毫秒 yyyy-MM-dd hh:mm:ss.sss
func GetNowTimeSSSStr() string {
	str := time.Now().Format(setting.AppSetting.DateFormat + ".000")
	return str
}

// 精确秒 yyyy-MM-dd hh:mm:ss
func GetNowTimeStr() string {
	str := time.Now().Format(setting.AppSetting.DateFormat)
	return str
}

var NowFunc = func() time.Time {
	return time.Now()
}
