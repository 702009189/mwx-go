package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"util"
)

// 主页
func IndexHtml(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", gin.H{
		"path": util.GetPath(c),
	})
}
