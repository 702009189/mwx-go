package routers

import (
	"github.com/gin-gonic/gin"
	"middleware/jwt"
	"net/http"
	"pkg/export"
	"pkg/logging"
	"pkg/setting"
	"pkg/upload"
	"routers/api"
	v1 "routers/api/v1"
	"routers/controller"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	r.Use(logging.GinLogger()) // 使用自定义日志
	r.Use(jwt.Core())          // 支持跨域

	r.Use(gin.Recovery())

	r.NoRoute(api.NoResponse) // 404

	// 静态资源
	r.StaticFS("/static", http.Dir(setting.AppSetting.LinuxRootPath+"src/view/static/")) // 网页静态文件
	r.StaticFS("/upload/images", http.Dir(upload.GetImageFullPath()))                    // 图片上传完整路径
	r.StaticFS("/export", http.Dir(export.GetExcelFullPath()))                           // 导出目录

	r.GET("/ping", api.Ping)     // ping
	r.GET("/login", api.GetAuth) // 登录认证
	r.POST("/auth", v1.AddAuth)  // 新增账号

	// 1.0 api
	apiv1 := r.Group("/api/v1")
	apiv1.Use(jwt.JWT()) // 认证拦截器
	{
		// 账号
		apiv1.GET("/auths", v1.GetAuths)             // 账号列表
		apiv1.GET("/currentAuth", v1.GetCurrentAuth) // 当前账号
		apiv1.GET("/auth/:id", v1.GetAuth)           // 根据id获取账号
		apiv1.PUT("/auth/:id", v1.EditAuth)          // 修改账号
		apiv1.DELETE("/auth/:id", v1.DeleteAuth)     // 删除账号
		apiv1.POST("/auth/export", v1.ExportAuth)    // 导出账号
		apiv1.POST("/auth/import", v1.ImportAuth)    // 导入账号

		// 部门
		apiv1.GET("/depts", v1.GetDepts)       // 部门列表
		apiv1.GET("/deptTree", v1.GetDeptTree) // 部门树形
	}

	// 上传
	upload := r.Group("/upload")
	{
		upload.POST("/image", api.UploadImage) // 上传图像
	}

	// 页面
	r.LoadHTMLGlob(setting.AppSetting.LinuxRootPath + "src/view/template/**/*")
	r.GET("/", controller.IndexHtml) // 主页
	return r
}
