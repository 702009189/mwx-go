package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"pkg/app"
	"pkg/e"
)

// 心跳
func Ping(c *gin.Context) {
	appG := app.Gin{C: c}
	appG.Response200("pong")
}

// 404
func NoResponse(c *gin.Context) {
	appG := app.Gin{C: c}
	appG.Response(http.StatusNotFound, e.NOT_FOUND, nil)
}
