package v1

import (
	"github.com/gin-gonic/gin"
	"pkg/app"
	"pkg/e"
	"service/dept_service"
)

// 部门列表
func GetDepts(c *gin.Context) {
	appG := app.Gin{C: c}
	deptService := dept_service.Dept{}
	depts, err := deptService.GetAll()
	if err != nil {
		appG.Response500(e.ERROR_GET_DEPTS_FAIL, nil)
	}
	appG.Response200(depts)
}

// 部门树形
func GetDeptTree(c *gin.Context) {
	appG := app.Gin{C: c}
	deptService := dept_service.Dept{}
	trees, err := deptService.GetTree(0)
	if err != nil {
		appG.Response500(e.ERROR_GET_DEPTS_FAIL, nil)
	}
	appG.Response200(trees)
}
