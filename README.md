# mwx-go

#### 介绍
go学习
702009189@qq.com

#### 软件架构
1. 使用 gin 轻量级框架
2. 本地访问地址 http://127.0.0.1:8000
3. GORM 中文文档 http://gorm.book.jasperxu.com/

#### 安装教程
环境变量
```
1. GOPATH 	G:\Go\GoWork
2. GOBIN 	G:\Go\jdk\bin
3. GOROOT	G:\Go\jdk\
4. Path 	G:\Go\jdk\bin
```
GoLand工具
```
5.  // gin
    go get github.com/gin-gonic/gin
   
    // mysql
    go get github.com/go-sql-driver/mysql
    
    // 日志 
    go get github.com/lestrrat/go-file-rotatelogs 
    go get github.com/rifflock/lfshook 
    go get github.com/sirupsen/logrus 
    
    // reids
    go get github.com/garyburd/redigo/redis
    
    // jwt验证
    go get github.com/dgrijalva/jwt-go
    
    // 工具类
    go get github.com/Unknwon/com
    
    // 日志
    go get github.com/sirupsen/logrus
    
    // excel 导出
    go get github.com/tealeg/xlsx
    
    // excel 导入
    go get github.com/360EntSecGroup-Skylar/excelize
8.  GoLand 环境配置
    Settings 
    Go->GOPATH->Module GOPATH 
        新增工程路径 G:\Go\GoWork\mwx-go
    // 程序入口
9.  Run/Debug Configurations->Templates->Go Bulid->Files 
        选中 G:\Go\GoWork\mwx-go\src\app\main.go
    // 编译目录      
    Run/Debug Configurations->Templates->Go Bulid->Output directory 
        选中 G:\Go\GoWork\mwx-go\runtime
10. 修改代码 # 1-linux or 2-windows
    app.ini ==> PathType = 1
    main.go ==> setting.Setup(1)        
```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
