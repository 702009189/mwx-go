-- 使用 mwx_go
USE `mwx_go`;
-- 账号
DROP TABLE IF EXISTS m_auth;
CREATE TABLE `m_auth` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) DEFAULT NULL COMMENT '用户名',
  `password` VARCHAR(255) DEFAULT NULL COMMENT '密码',
  `mobile` VARCHAR(20) DEFAULT NULL COMMENT '手机号',
  `deleted_on`  INT(10) DEFAULT '0' COMMENT '软删除标志:0-有效,!=0秒级时间戳',
  `created_on` TIMESTAMP(3) NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
  `created_by` VARCHAR(255) DEFAULT NULL COMMENT '创建人',
  `modified_on` TIMESTAMP(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '修改时间',
  `modified_by` VARCHAR(255) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='账号';
-- 创建账号
insert m_auth(`username`, `password`, `mobile`) values('maran', 'maran123', '17801010000');
insert m_auth(`username`, `password`, `mobile`) values('u1', 	'p1', 		'17601010001');
insert m_auth(`username`, `password`, `mobile`) values('u2', 	'p2', 		'17601010002');
insert m_auth(`username`, `password`, `mobile`) values('u3', 	'p3', 		'17601010003');
insert m_auth(`username`, `password`, `mobile`) values('u4', 	'p4', 		'17601010004');
insert m_auth(`username`, `password`, `mobile`) values('u5', 	'p5', 		'17601010005');
insert m_auth(`username`, `password`, `mobile`) values('u6', 	'p6', 		'17601010006');
insert m_auth(`username`, `password`, `mobile`) values('u7', 	'p7', 		'17601010007');
insert m_auth(`username`, `password`, `mobile`) values('u8', 	'p8', 		'17601010008');
insert m_auth(`username`, `password`, `mobile`) values('u9', 	'p9', 		'17601010009');
insert m_auth(`username`, `password`, `mobile`) values('u10',	'p10', 		'17601010010');
insert m_auth(`username`, `password`, `mobile`) values('u11',	'p11', 		'17601010011');
insert m_auth(`username`, `password`, `mobile`) values('u12',	'p12', 		'17601010012');
insert m_auth(`username`, `password`, `mobile`) values('u13',	'p13', 		'17601010013');
insert m_auth(`username`, `password`, `mobile`) values('u14',	'p14', 		'17601010014');
insert m_auth(`username`, `password`, `mobile`) values('u15',	'p15', 		'17601010015');
insert m_auth(`username`, `password`, `mobile`) values('u16',	'p16', 		'17601010016');
insert m_auth(`username`, `password`, `mobile`) values('u17',	'p17', 		'17601010017');
insert m_auth(`username`, `password`, `mobile`) values('u18',	'p18', 		'17601010018');
insert m_auth(`username`, `password`, `mobile`) values('u19',	'p19', 		'17601010019');
insert m_auth(`username`, `password`, `mobile`) values('u20',	'p20', 		'17601010020');
insert m_auth(`username`, `password`, `mobile`) values('u21',	'p21', 		'17601010021');
insert m_auth(`username`, `password`, `mobile`) values('u22',	'p22', 		'17601010022');
insert m_auth(`username`, `password`, `mobile`) values('u23',	'p23', 		'17601010023');
insert m_auth(`username`, `password`, `mobile`) values('u24',	'p24', 		'17601010024');
insert m_auth(`username`, `password`, `mobile`) values('u25',	'p25', 		'17601010025');
insert m_auth(`username`, `password`, `mobile`) values('u26',	'p26', 		'17601010026');
insert m_auth(`username`, `password`, `mobile`) values('u27',	'p27', 		'17601010027');
insert m_auth(`username`, `password`, `mobile`) values('u28',	'p28', 		'17601010028');
insert m_auth(`username`, `password`, `mobile`) values('u29',	'p29', 		'17601010029');
insert m_auth(`username`, `password`, `mobile`) values('u30',	'p30', 		'17601010030');
insert m_auth(`username`, `password`, `mobile`) values('u31',	'p31', 		'17601010031');
insert m_auth(`username`, `password`, `mobile`) values('u32',	'p32', 		'17601010032');
insert m_auth(`username`, `password`, `mobile`) values('u33',	'p33', 		'17601010033');
insert m_auth(`username`, `password`, `mobile`) values('u34',	'p34', 		'17601010034');
insert m_auth(`username`, `password`, `mobile`) values('u35',	'p35', 		'17601010035');
insert m_auth(`username`, `password`, `mobile`) values('u36',	'p36', 		'17601010036');

-- 部门
DROP TABLE IF EXISTS m_dept;
CREATE TABLE `m_dept` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) DEFAULT NULL COMMENT '父id',
  `name` VARCHAR(255) DEFAULT NULL COMMENT '部门名称',
  `deleted_on`  INT(10) DEFAULT '0' COMMENT '软删除标志:0-有效,!=0秒级时间戳',
  `created_on` TIMESTAMP(3) NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
  `created_by` VARCHAR(255) DEFAULT NULL COMMENT '创建人',
  `modified_on` TIMESTAMP(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '修改时间',
  `modified_by` VARCHAR(255) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='部门';
-- 创建部门
insert m_dept(`id`, `p_id`, `name`) values(1,       0,      '飞马科技');
insert m_dept(`id`, `p_id`, `name`) values(2,       1,      '襄阳总公司');
insert m_dept(`id`, `p_id`, `name`) values(3,       2,      '研发部门');
insert m_dept(`id`, `p_id`, `name`) values(4,       2,      '市场部门');
insert m_dept(`id`, `p_id`, `name`) values(5,       2,      '测试部门');
insert m_dept(`id`, `p_id`, `name`) values(6,       2,      '财务部门');
insert m_dept(`id`, `p_id`, `name`) values(7,       2,      '运维部门');
insert m_dept(`id`, `p_id`, `name`) values(9,       1,      '深圳分公司');
insert m_dept(`id`, `p_id`, `name`) values(10,      9,      '市场部门');
insert m_dept(`id`, `p_id`, `name`) values(11,      9,      '财务部门');

-- last update time 2019年5月29日14:49:52


